### Установка бота

1. Клонируем репозиторий `git clone https://b4rbed@bitbucket.org/b4rbed/exmo-bot.git`
2. Заходим в папку `cd exmo-bot`
3. Запускаем установку пакетов `./install.sh`
4. Добавляем еще 2 пакета `pip3 install numpy` и `pip3 install TA-lib`
5. Запускаем скрипт `python3 exmo.py`
