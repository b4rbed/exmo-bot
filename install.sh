#!/bin/bash

yes Y | apt-get update;
yes Y | apt-get install python3-venv;

python3 -m venv env;
source ./env/bin/activate;

wget "http://prdownloads.sourceforge.net/ta-lib/ta-lib-0.4.0-src.tar.gz";
tar xvfz ta-lib-0.4.0-src.tar.gz;
cd ta-lib;
apt-get install gcc;
./configure --prefix=/usr;
apt install make;
make;
sudo make install;

yes Y | apt install python3-pip;
pip3 install requests;
pip3 install matplotlib;
pip3 install numpy;
pip3 install TA-lib; 
